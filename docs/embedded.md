# 2. Embedded solution

###### Embedded Programming List 


## 2.1 Objectives

<Provide background and reasoning what needs to be solved here, why this solution was choosen and what steps need to be taken>


## 2.3 Steps taken

![alt text]()

#### Components used 


## 2.4 Testing & Problems


## 2.5 Proof of work

< Add video and/or photos of your tests and proof it works!>

## 2.6 Files & Code

<Add your embedded code and other relevant documents in a ZIp file under the /files folder and link here.>

## 2.7 References & Credits

<add all used references & give credits to those u used code and libraries from>
