# 1. Project Title: GearTracks

                                ### Image of Architecture Here

## Introduction 

<!-- Output copied to clipboard! -->

<!-----
NEW: Check the "Suppress top comment" option to remove this info from the output.

Conversion time: 8.118 seconds.


Using this Markdown file:

1. Paste this output into your source file.
2. See the notes and action items below regarding this conversion run.
3. Check the rendered output (headings, lists, code blocks, tables) for proper
   formatting and use a linkchecker before you publish this page.

Conversion notes:

* Docs to Markdown version 1.0β29
* Mon May 17 2021 17:51:11 GMT-0700 (PDT)
* Source doc: GearTracks
* Tables are currently converted to HTML tables.
* This document has images: check for >>>>>  gd2md-html alert:  inline image link in generated source and store images to your server. NOTE: Images in exported zip file from Google Docs may not appear in  the same order as they do in your doc. Please check the images!


WARNING:
You have 2 H1 headings. You may want to use the "H1 -> H2" option to demote all headings by one level.

----->


<p style="color: red; font-weight: bold">>>>>>  gd2md-html alert:  ERRORs: 0; WARNINGs: 1; ALERTS: 28.</p>
<ul style="color: red; font-weight: bold"><li>See top comment block for details on ERRORs and WARNINGs. <li>In the converted Markdown or HTML, search for inline alerts that start with >>>>>  gd2md-html alert:  for specific instances that need correction.</ul>

<p style="color: red; font-weight: bold">Links to alert messages:</p><a href="#gdcalert1">alert1</a>
<a href="#gdcalert2">alert2</a>
<a href="#gdcalert3">alert3</a>
<a href="#gdcalert4">alert4</a>
<a href="#gdcalert5">alert5</a>
<a href="#gdcalert6">alert6</a>
<a href="#gdcalert7">alert7</a>
<a href="#gdcalert8">alert8</a>
<a href="#gdcalert9">alert9</a>
<a href="#gdcalert10">alert10</a>
<a href="#gdcalert11">alert11</a>
<a href="#gdcalert12">alert12</a>
<a href="#gdcalert13">alert13</a>
<a href="#gdcalert14">alert14</a>
<a href="#gdcalert15">alert15</a>
<a href="#gdcalert16">alert16</a>
<a href="#gdcalert17">alert17</a>
<a href="#gdcalert18">alert18</a>
<a href="#gdcalert19">alert19</a>
<a href="#gdcalert20">alert20</a>
<a href="#gdcalert21">alert21</a>
<a href="#gdcalert22">alert22</a>
<a href="#gdcalert23">alert23</a>
<a href="#gdcalert24">alert24</a>
<a href="#gdcalert25">alert25</a>
<a href="#gdcalert26">alert26</a>
<a href="#gdcalert27">alert27</a>
<a href="#gdcalert28">alert28</a>

<p style="color: red; font-weight: bold">>>>>> PLEASE check and correct alert issues and delete this message and the inline alerts.<hr></p>




<p id="gdcalert1" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image1.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert2">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image1.jpg "image_tooltip")


<p id="gdcalert2" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image2.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert3">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image2.jpg "image_tooltip")


** **

**Competition :**

**Registration **

**Individual**

**[https://forms.gle/MBShyxs4iVLNYNNz5](https://forms.gle/MBShyxs4iVLNYNNz5) **

**Team**

**[https://tinyurl.com/cgh2021teams](https://tinyurl.com/cgh2021teams) **

** **Cairo Yael L.E.

[Yael.l.e.cairo@gmail.com](mailto:Yael.l.e.cairo@gmail.com) 

+597 850/7826

Darlene Gefferie

[darlenegeff@hotmail.com](mailto:darlenegeff@hotmail.com) 

+5978809774

Anjessa Linger

[anjessalinger1@gmail.com](mailto:anjessalinger1@gmail.com)

5978702252

Devika Mahabir

5978758875

[sarishma.mahabir@gmail.com](mailto:sarishma.mahabir@gmail.com)

Faith Pherai

[angelipherai@gmail.com](mailto:angelipherai@gmail.com) 

5978639690 

Julie Sundar

[juliesundar123@gmail.com](mailto:juliesundar123@gmail.com)

5978822698 

100 teams 12 countries max 500 students 

Pitch 100 to 30 shortlist 

How impactful - 10 points 

Solution  why you selected that theme?

Target group 25 

Pitch content 60 points quality of pitch is 10 points

Tech is 40 points

Value prop 10 points.

Articulation and language 5 points

**Phase 1**

Create a new logo

Generate new intro video using Viyydious logo generator

Audio for existing GearTracks video needs to be written down - Anjessa

Redo GearTracks Audio - Faith 

Google slides for GearTracks - Devika 

Ideation Pitch Sunday  - Anjessa 

Content added to slides - Everyone 

Understand what your talking about - All of the above

_Link to GearTracks Video - _[https://www.youtube.com/watch?v=T_eN6UyARsc](https://www.youtube.com/watch?v=T_eN6UyARsc) 

_Link to GearTrack Slide - [https://docs.google.com/presentation/d/1wONQyD4DOnFocwTZsBl3q00Zcf7nsfvZTpB9g8CyyJs/edit?usp=sharing](https://docs.google.com/presentation/d/1wONQyD4DOnFocwTZsBl3q00Zcf7nsfvZTpB9g8CyyJs/edit?usp=sharing) _

_Business Model Canvas : [https://canvanizer.com/canvas/w73C55ymVk4Wj](https://canvanizer.com/canvas/w73C55ymVk4Wj) _

**Phase 2**

Agree on Architecture 

Building of prototype 

	Embedded System - Anjessa + Julie + Darlene

	Mobile App - Faith + Julie

	Enclosure Design - Devika + Yael

	Animation video plus live demo - Faith Devika Julie Yael


## Team Fusion Girls

** **Cairo Yael L.E.

[Yael.l.e.cairo@gmail.com](mailto:Yael.l.e.cairo@gmail.com) 

+597 850/7826

Darlene Gefferie

[darlenegeff@hotmail.com](mailto:darlenegeff@hotmail.com) 

+5978809774

Anjessa Linger

[anjessalinger1@gmail.com](mailto:anjessalinger1@gmail.com)

5978702252

Devika Mahabir

5978758875

[sarishma.mahabir@gmail.com](mailto:sarishma.mahabir@gmail.com)

Faith Pherai

[angelipherai@gmail.com](mailto:angelipherai@gmail.com) 

5978639690 

Julie Sundar

[juliesundar123@gmail.com](mailto:juliesundar123@gmail.com)

5978822698 


## Challenge

GearTracks aims at finding a preventive solution for "ghost gear", lost gear ending up in the oceans, which results in loss of thousands of dollars and damage to marine life. Instead of finding solutions after the damage has occurred we provide you with a framework to prevent the problems from even starting. While most focus on tracking and monitoring vessels we aim to track the fishing gear which is the actual source of the problem.


## Solution



*   GearTracks is a gear tracking and monitoring framework based on incorporating a low cost Internet of Things Solution for fisheries.
*   GearTracks provides users with devices called flutters and a dashboard with which they can monitor the usage, state, and position of their fishing gear. It is built in such a way to be "set and forget" and unobtrusive to the normal fisheries process.
*   Flutters consists of an esp32 microcontroller capable of both wifi and bluetooth. Attached to the esp32 is a pressure sensor used to detect the state of the flutter device. When the status under water is detected the esp32 will trigger the emploding of the co2 capsule causing the flutter to float to the surface.
*   The status is sent to the fisherman over either bluetooth or wifi.


## GearTracks Flutters

Flutters come in two types, one for fishnets and one for crab cages. Our prototype was built mostly from recycled products like PVC tubes and fittings combined with generally available consumables like CO2 tanks and braided high strength fish line. Flutters connect locally to their IoT gateway (raspberry pi) which also hosts the dashboard and connects to the internet.Users can locally monitor and store their data until connectivity to the internet is available where their data will be stored in the cloud.


## Obstacles



*   Building IoT firmware, full back-end infrastructure and front end stack in the time frame of 48 hours
*   Physically fitting all the parts plus IoT hardware into a standard fishing buoy
*   Finding solution for underwater connectivity


## Future

We are currently working on completing the first prototype of our flutters towards the first release (some miniaturization required) and the finalization of the cloud and user software.



<p id="gdcalert3" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image3.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert4">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image3.png "image_tooltip")




<p id="gdcalert4" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image4.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert5">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image4.png "image_tooltip")


**Scripts: **

**Script for animation video **

Faith and Julie

Did you know that there are about 640000 tons of lost fishing gears in the oceans

It is caused either by bad weather which causes fishing nets to fall into the sea.

Fisherman throwing broken nets off their boats because their boats have no space or by boats accidentally cutting off lobster traps and nets while sailing .

Ghosts gears causes governments and fishermen to loose thousands of dollars yearly for finding & cleaning up ghosts gears and fishermen having to buy new gears. But the worst problem of them all is marine lives being caught in nets which results in no fish harvesting , no fish repopulation and global food loss.

Therefore we introduce to you GearTracks

GearTracks is a gear tracking and monitoring system 

IT consists of devices called flutters which comes in two units flutters for nets and flutters for crab cages.

Flutters tracks your gps coordinates and monitors the states of your gear.

The states are sleep mode when not in use 

Floating when water sensor detects water

And under water when pressure sensor detects change in pressure 

For worse case flutters can be preconfigured 

For example to alarm on connectivity loss to which it sends a distress signal to the system with its last timestamp and location or to inflate and float to the surface after a specific period of time.

If someone should find a flutter with a simple picture of its barcode. They will be redirected to the QR page

Where they would submit their findings to possibly claim an award from the owner.

Did you know that there is about 640.000 tons of lost fishing gear in the ocean?

It is caused either by: 



*    bad weather which causes fishing nets to fall into the sea.
*   Fisherman throwing broken nets off their boats because their boats have no space or by boats accidentally cutting off lobster traps and nets while sailing

     .


Ghosts gear causes governments and fishermen to loose thousands of dollars yearly for finding, cleaning and buying new gear.  

But the worst problem of them all is, the marine live getting caught up/stuck in nets which results in:



*   no fish harvesting  
*   no fish repopulation 
*   and global food loss.

**Therefore we introduce to you GearTracks. **

GearTracks is a gear tracking and monitoring framework. It consists of devices called flutters which come in two units:



*   flutters for nets 
*   and flutters for crab cages. 

Flutters tracks your gps coordinates and monitors the state of you gear.

The states are:

- sleep mode. when not in use. 

- Floating. when water sensor detect the water. 

- And under water. when pressure sensor detects the change in pressure. 

 

For worse cases flutters can be preconfigured. For example; the framework detects and  alarms a connectivity loss of the flutter with it’s last location and time stamp. 

and the flutters inflate and float to the surface after a specific periode of time. 

if someone should find a flutter, with a simple picture of it’s barcode they will be redirected

to the registration website where they can submit there findings to possibly claim an award from the owner. So,... flutters is‘the’ tool  to register all gear that will be used and could potentialy be lost at sea.

**Script for Presentation **

Anjessa Linger 

**Script**

There are several issues plaguing the ocean but the issue we want to tackle is ghost gear. Ghost gear cost lots of issues namely capturing marine life even when they’re not in use anymore. 

Damaging the habitat for example coral reefs are the natural habitat and food source for lots of marine life but ghost gear can ruin them which in turn disturbs the ecosystem

Our solution to ghost gear is gear tracks a gear monitoring and tracking system 

Video plays 

In every flutter there will be a controller with two sensors that monitor the state of your flutter 

You can connect your phone to the flutter to check the state of it 

Gear tracks is the tool for fishers , fleet owners and legislators 

We introduce ourselves 

Gear tracks your ghost gear buster

**INFORMATION **

“Ghost fishing” is a part of the global marine debris issue that impacts marine organisms and the environment. Lost or discarded fishing gear that is no longer under a fisherman’s control becomes known as derelict fishing gear (DFG), and it can continue to trap and kill fish, crustaceans, marine mammals, sea turtles, and seabirds. The most common types of DFG to ghost fish are gillnets and crab pots/traps, with longlines and trawls less likely to do so. Ghost fishing can impose a variety of harmful impacts, including: the ability to kill target and non-target organisms, including endangered and protected species; causing damage to underwater habitats such as coral reefs and benthic fauna; and contributing to marine pollution. Factors that cause gear to become DFG include poor weather conditions, gear conflicts with other vessels or bottom topography, gear overuse, and too much gear being used.

**Research**

**Embedded Circuit and Research**

Workday 1 March 31 2021 

Anjess  Linger

Arduino Nano Hc05 sending A0 data to Arduino Bluetooth Data app 

[https://www.instructables.com/How-to-Receive-Arduino-Sensor-Data-on-Your-Android/](https://www.instructables.com/How-to-Receive-Arduino-Sensor-Data-on-Your-Android/)

[https://www.electronics-lab.com/get-sensor-data-arduino-smartphone-via-bluetooth/](https://www.electronics-lab.com/get-sensor-data-arduino-smartphone-via-bluetooth/)

Today we built the electronic circuit for geartracks( the pressure sensor has not been added yet)

We added the hc05 bluetooth module to the arduino nano and send bogus analog data over bluetooth to an existing android app

Picture : \


<p id="gdcalert5" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image5.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert6">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image5.png "image_tooltip")


Julie Sundar



<p id="gdcalert6" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image6.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert7">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image6.png "image_tooltip")




<p id="gdcalert7" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image7.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert8">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image7.png "image_tooltip")


[https://www.adafruit.com/product/1603](https://www.adafruit.com/product/1603) 



<p id="gdcalert8" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image8.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert9">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image8.png "image_tooltip")


<p id="gdcalert9" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image9.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert10">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image9.png "image_tooltip")




<p id="gdcalert10" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image10.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert11">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image10.png "image_tooltip")




1. Altitude in meter and feet 
2. Temperature in Celcius and Farenheit
3. Absolute pressure mb and inMg
4. Relative Pressure (sea- level)
5. Computed Altitude in meter and feet 

Doing 

[https://randomnerdtutorials.com/guide-for-bmp180-barometric-sensor-with-arduino/](https://randomnerdtutorials.com/guide-for-bmp180-barometric-sensor-with-arduino/) 

Run SEF_BMP180 Example to test sensor on nano

**Mobile App and Research**

Julie Sundar

Mobile app no code todo

[https://x.thunkable.com/projects](https://x.thunkable.com/projects) 

Thunkable bluetooth

[https://youtu.be/sNpCTsVifo4](https://youtu.be/sNpCTsVifo4) 

Thunkable map

[https://youtu.be/24QiXACNIcY](https://youtu.be/24QiXACNIcY) 

Darlene Gefferie


# Mobile App Ble


# [https://www.youtube.com/watch?v=a1S-abFOZMA](https://www.youtube.com/watch?v=a1S-abFOZMA)

[https://www.youtube.com/watch?v=irr3c46HvDU](https://www.youtube.com/watch?v=irr3c46HvDU)

Arduino and Android BlueTooth monitor Using BMP180

[https://www.youtube.com/watch?v=sIwVAqoQS4U](https://www.youtube.com/watch?v=sIwVAqoQS4U)

Application

1.	Home page (geartrack logo)

2.	Found a flutter, Sign-up or Log-in: (user & founder*)

a. sign-up Email, password & name

b. log-in username/email & password 

c. Found a flutter: scan qr-code (camera)

User

3.	Register your gear

a. Scan QR-code (camera)

Type: (Fishnet or crabcage)


    When someone finds your gear they can contact you (optional)

Name:

Number or mail:

Add another gear

4.	Info Flutter

a. Location

b. State

5.	Location

Map with pin where the flutter(s) is/are located

6.	State/ Status 


    - sleeping state

In use

- Floating

-Underwater

7.	Lost flutter

For example to alarm on connectivity loss to which it sends a distress signal to the system with its last timestamp and location or to inflate and float to the surface after a specific period of time.If someone should find a flutter with a simple picture of its barcode. They will be redirected to the QR page. Where they would submit their findings to possibly claim an award from the owner.

8.	Signal if someone found a flutter

Founder

3.	Scan QR-code (camera)

a. get owner info:

Type: (Fishnet or crabnet)

Name:

Number or mail:

or

b. Notify owner that his/her flutter is found

location of flutter (or founder can) will be signaled to the owner

•	About us

Fusion girls

what/why geartrack

•	Contact us

website 

socialmedia

number

mail

•	Ask us/ Questions?



<p id="gdcalert11" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image11.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert12">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image11.png "image_tooltip")


Faith Pherai

BLE coded thunkable project

[https://x.thunkable.com/projectPage/5d27696912df12e390d05d9a](https://x.thunkable.com/projectPage/5d27696912df12e390d05d9a) 

Make a sign up page 

[https://www.youtube.com/watch?v=M9XVdLU25fQ&ab_channel=Thunkable](https://www.youtube.com/watch?v=M9XVdLU25fQ&ab_channel=Thunkable) 

App functions

Top priority:



*   User account:user must be able to register or make their account, and login with email/password
*    Register gear:scan QR code register( open camera)  or manual input
*   Track gear position: choose and put on the map
*   User profile:update on state of registered gear on map via wifi or BLE
*   Show ghost gear info when scanning QRcode

Second priority:



*   Upload profile picture
*   Upload and attach gear picture
*   Contact owner if gear is found



<p id="gdcalert12" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image12.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert13">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image12.jpg "image_tooltip")


Sign up page example



<p id="gdcalert13" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image13.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert14">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image13.png "image_tooltip")


Install Android Thunkable Live

 **Mobile App No Code Builder**

**Instal Lightblue App**

**Name BLE device: HC-05**

Mobile App test here  Thunker .. Bluetooth 

[https://youtu.be/sNpCTsVifo4](https://youtu.be/sNpCTsVifo4) 

[https://youtu.be/24QiXACNIcY](https://youtu.be/24QiXACNIcY)  .. Map 

6 april 2021

**Sign up in bubble.io**

[https://learn.bubbleapps.io/version-p_1617726122902x409793329854444240?debug_mode=true](https://learn.bubbleapps.io/version-p_1617726122902x409793329854444240?debug_mode=true) **Map made with bubble.io **

<p id="gdcalert14" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image14.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert15">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image14.jpg "image_tooltip")


**Bleutooth WIth API on bubble.io [https://www.youtube.com/watch?v=qJg2DzquPdw](https://www.youtube.com/watch?v=qJg2DzquPdw) **

**[https://cordova.apache.org/](https://cordova.apache.org/) **

**[https://github.com/mapsplugin/cordova-plugin-googlemaps](https://github.com/mapsplugin/cordova-plugin-googlemaps) **

**## Julie Sundar **

**Testing Cordova **



1. **Installing nodejs on windows 10 **
2. **Uninstall all previous node installation from windows os**
3. **Install node from site [https://nodejs.org/en/download/](https://nodejs.org/en/download/) 64 bit **
4. **Install nvm for managing nodejs and npm versions [https://github.com/coreybutler/nvm-windows/releases](https://github.com/coreybutler/nvm-windows/releases) nvm setup zip **
5. **Cmdline do nvm install npm g**
6. **Nvm intall 12.0.0 npm 6.9.0**
7. 
8. **<code>sudo npm install -g cordova</code></strong>
9. <strong><code>npm install -g cordova</code></strong>
10. <strong><code>cordova create hello com.example.hello HelloWorld</code></strong>
11. <strong><code>cd hello</code></strong>
12. <strong><code>cordova platform add ios</code></strong>
13. <strong><code>$ cordova platform add android</code></strong>
14. <strong><code>cordova platform ls</code></strong>
15. <strong><code>cordova build</code></strong>
16. <strong><code>cordova emulate android</code></strong>
17. 
18. <strong>Checking Java JDK and Android SDK versions</strong>
19. <strong>ANDROID_SDK_ROOT=undefined (recommended setting)</strong>
20. <strong>ANDROID_HOME=undefined (DEPRECATED)</strong>
21. <strong>Failed to find 'ANDROID_SDK_ROOT' environment variable. Try setting it manually.</strong>
22. <strong>Failed to find 'android' command in your 'PATH'. Try update your 'PATH' to include path to valid SDK directory.</strong>

<strong>## Trouble shoot</strong>

**[https://javatutorial.net/set-java-home-windows-10](https://javatutorial.net/set-java-home-windows-10) **



<p id="gdcalert15" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image15.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert16">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image15.jpg "image_tooltip")


<p id="gdcalert16" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image16.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert17">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image16.jpg "image_tooltip")


<p id="gdcalert17" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image17.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert18">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image17.jpg "image_tooltip")



## 4. Test your configuration

**Open a new command prompt and type in:**


```
echo %JAVA_HOME%
```


**this will print out the directory JAVA_HOME points to or empty line if the environment variable is not set correctly**

**Now type in: \
Javac -version**

**Checking Java JDK and Android SDK versions**

**ANDROID_SDK_ROOT=undefined (recommended setting)**

**ANDROID_HOME=undefined (DEPRECATED)**

**Using Android SDK: C:\Users\julie\AppData\Local\Android\sdk**

**Could not find an installed version of Gradle either in Android Studio,**

**or on your system to install the gradle wrapper. Please include gradle**

**in your path, or install Android Studio**

**Download gradle binary only [https://gradle.org/releases/](https://gradle.org/releases/) **

**Extract file and place it in**



<p id="gdcalert18" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image18.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert19">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image18.png "image_tooltip")


**Copy and paste path in **



<p id="gdcalert19" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image19.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert20">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image19.png "image_tooltip")


<p id="gdcalert20" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image20.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert21">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image20.png "image_tooltip")


**Then run_ cordova build _**

**[https://www.freakyjolly.com/resolve-could-not-find-an-installed-version-of-gradle-either-in-android-studio/](https://www.freakyjolly.com/resolve-could-not-find-an-installed-version-of-gradle-either-in-android-studio/) **



23. **[https://developer.android.com/studio](https://developer.android.com/studio) **
24. **Cordova install as nodejs project [https://cordova.apache.org/docs/en/10.x/guide/cli/](https://cordova.apache.org/docs/en/10.x/guide/cli/) **

	**3D Design and Research**

Devika Mahabir

Todo: Searching for or purchasing Floats for enclosure 

Example 3D design float : [https://www.thingiverse.com/thing:995844](https://www.thingiverse.com/thing:995844)

**Script for Animation Video 2.0**

Anjessa and Darline

Scene 1:

Mother digging hole, lays eggs

Scene 2:

The eggs start hatching meanwhile in the ocean at sea storm different ways nets and crab cages get into the ocean 

Scene 3:

The turtles go into the ocean but one is a bit slower than the rest mom helps him

Scene 4:

They go into the ocean the turtle is behind in the ocean (show nets etc that pollute the ocean)

He gets stuck into a net but the mom is too busy to see that he’s behind he tries to make a sound to notify her but she cant hear him because he’s to far away

Scene 5:

When they arrive at their destination the mom realizes one of her kids is missing. She goes looking for him everywhere but she can't find him. Finally she finds him but he’s stuck in the net on a rock so she can't bring him with her. She tries to save him but it doesn't work then she gets different animals to try and help but they can't help. That night she stays with the turtle and the have one last touch (sad music ) 

This is the reality for lots of marine life, ghost gear continues to ruin our ocean but, you can make a difference by using gear tracks 

Talk about geartracks 

Demo 

At the end 

Our oceans are dying route 

At the end -> the ball is now in your court

**Second iteration**

**Visuals**

Scene 1 :

There’s a storm at sea and that's how the net ends in the ocean

Scene 2: a mother turtle and her child are swimming around and the baby turtle gets stuck inside of the net 

Scene 3: She tries to save him but it doesn't work then she gets different animals to try and help but they can't help. That night she stays with the turtle and the have one last touch (sad music ) 

**Visuals**

Did you know that there are about 640000 tons of lost fishing gears in the oceans

It is caused either by bad weather which causes fishing nets to fall into the sea.

Fishermen throwing broken nets off their boats because their boats have no space or by boats accidentally cutting off lobster traps and nets while sailing .

**Lost fishing gear or as it is called** Ghosts gears causes governments and fishermen to lose thousands of dollars yearly for finding & cleaning up ghost gears and fishermen having to buy new gears. But the worst problem of them all is marine lives being caught in nets 

**Thunder storm Zoom in Story start story time (flip book)**

**Song play **

**have the story of the turtle because it fits really well**

(maybe skip this which results in no fish harvesting , no fish repopulation and global food loss.)

(**but we have a solution to this problem it's called geartracks)**Therefore we introduce to you GearTracks

GearTracks is a gear tracking and monitoring system 

It consists of devices called flutters which comes in two units flutters for nets and flutters for crab cages.

Flutters tracks your gps coordinates and monitors the states of your gear.

The states are sleep mode when not in use 

Floating when water sensor detects water

And under water when pressure sensor detects change in pressure 

For worse case flutters can be preconfigured 

For example to alarm on connectivity loss to which it sends a distress signal to the system with its last timestamp and location or to inflate and float to the surface after a specific period of time.

If someone should find a flutter with a simple picture of its barcode. They will be redirected to the QR page

Where they would submit their findings to possibly claim an award from the owner. **So by using geartracks we can bring a start to ending ghost gear (something like that end on a positive note)**

Note: Make ur own music anjessa and faith : 

**Photoshoot**



<p id="gdcalert21" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image21.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert22">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image21.png "image_tooltip")


<p id="gdcalert22" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image22.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert23">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image22.png "image_tooltip")




<p id="gdcalert23" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image23.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert24">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image23.png "image_tooltip")


<p id="gdcalert24" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image24.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert25">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image24.png "image_tooltip")


<p id="gdcalert25" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image25.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert26">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image25.png "image_tooltip")



<table>
  <tr>
   <td><strong>ANJESSA</strong>
   </td>
   <td><strong>DEVIKA</strong>
   </td>
   <td><strong>FAITH</strong>
   </td>
   <td><strong>YAEL</strong>
   </td>
   <td><strong>DARLENE</strong>
   </td>
  </tr>
  <tr>
   <td>
<ul>

<li>Write official script in Google docs and send it to friend. (Today 11 april evening) \
 \


<li>Julie Task:

<li>Continue with Flipbook

<li>Work on the song
</li>
</ul>
   </td>
   <td>
<ul>

<li>Color scheme for coloring sketches. (Tomorrow 12 april  morning)
    <a href="https://coolors.co/463f3a-8a817c-bcb8b1-f4f3ee-e0afa0">https://coolors.co/463f3a-8a817c-bcb8b1-f4f3ee-e0afa0</a>
<p>

    <a href="https://www.eggradients.com/?73afe765_page=2">https://www.eggradients.com/?73afe765_page=2</a>
<p>

    Julie Task:
<p>

    Search for animated gifs for fishermen and whatever gifs is needed for additional overlay
</li>
</ul>
   </td>
   <td>
<ul>

<li>Record about GT as in previous video according to video 2. (Thursday 15 april)
<ul>

<li>Julie Task;

<li>Adola mobile app screen mockups

<li>Work on the song
</li>
</ul>
</li>
</ul>
   </td>
   <td>
<ul>

<li>Has a camera so she can take pictures of the drawings and scan it. (Right away when we get the sketches

<li>

<li>Julie Task

<li>Overlay animated fit onto existing Geartracks video using adobe premier )
</li>
</ul>
   </td>
   <td>
<ul>

<li>Song with Anjessa and Faith (Wednesday 14 april
         Julie Task
<p>
Assist Faith or Devika 
<p>
Work on how we will present the demo using live flutter and mockup screen of app
</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>
<ul>

<li>Responsible for getting the sketches done by friend. (Sketches done by monday midnight) 
    (If not than Tuesday 13 april evening)
</li>
</ul>
   </td>
   <td>
<ul>

<li>Color & edit (add shading to look more realistic etc) sketches in Photoshop. (Right away after Sketches are scanned)
    (Done by Friday 16 april midnight) 
</li>
</ul>
   </td>
   <td>
<ul>

<li>Color & edit (add shading to look more realistic etc) sketches in Photoshop. (Right away after Sketches are scanned)
    (Done by Friday 16  midnight)
</li>
</ul>
   </td>
   <td>
<ul>

<li>Color & edit (add shading to look more realistic etc) sketches in Photoshop. (Right away after Sketches are scanned)
    (Done by Friday 16 april midnight)
</li>
</ul>
   </td>
   <td>
<ul>

<li>Color & edit (add shading to look more realistic etc) sketches in Photoshop. (Right away after Sketches are scanned)
    (Done by Friday 16 april midnight)
</li>
</ul>
   </td>
  </tr>
  <tr>
   <td>
<ul>

<li>Song with Darlene and Faith.
    (Wednesday 14 april)
</li>
</ul>
   </td>
   <td>
<ul>

<li>Responsible for checking how far the team members are with the task and check deadline. (Everyday)
</li>
</ul>
   </td>
   <td>
<ul>

<li>Edit video (stop motion) in Adobe Premiere with Yael. 
    (Sunday 18 april evening)
</li>
</ul>
   </td>
   <td>
<ul>

<li>Edit video (stop motion) in Adobe Premiere with Faith. 
    (Sunday 18 april evening)
</li>
</ul>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>
<ul>

<li>Prepare for pitch. Also written Poem for the pitch.
    24 APRIL FINAL
</li>
</ul>
   </td>
   <td>
<ul>

<li>Edit slides if needed. (Sunday 18 april)
</li>
</ul>
   </td>
   <td>
<ul>

<li>Song with Anjessa and Darlene. Wednesday 14 april)
</li>
</ul>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
</table>



<table>
  <tr>
   <td>

<p id="gdcalert26" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image26.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert27">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


<img src="images/image26.png" width="" alt="alt_text" title="image_tooltip">

   </td>
   <td>

<p id="gdcalert27" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image27.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert28">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


<img src="images/image27.png" width="" alt="alt_text" title="image_tooltip">

   </td>
   <td>

<p id="gdcalert28" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image28.jpg). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert29">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


<img src="images/image28.jpg" width="" alt="alt_text" title="image_tooltip">

   </td>
  </tr>
  <tr>
   <td>#713421 turtle shield
   </td>
   <td>#157702
   </td>
   <td>0092f0 water
   </td>
  </tr>
  <tr>
   <td>#8CD11C turtle skin
   </td>
   <td>#7D3B0B
   </td>
   <td>71b9ef Gradient sky
   </td>
  </tr>
  <tr>
   <td>#FDBD43 Under shield
   </td>
   <td>#349B0B
   </td>
   <td>B7c402 boat
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>#74D019
   </td>
   <td>Ff8400 boat
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>#53BA01
   </td>
   <td>E00606 boat
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>
   </td>
   <td>Fff200 Bliksem
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
</table>


Animated Gifs to overlay in Adobe Premiere Links : 



*    Fishing net 

[https://media1.giphy.com/media/iHE6olkAGTtvmThbLT/200w.gif?cid=82a1493bp2tjo17hqj6kes9cmdt473n6o96vh3xr7ym7qhct&rid=200w.gif](https://media1.giphy.com/media/iHE6olkAGTtvmThbLT/200w.gif?cid=82a1493bp2tjo17hqj6kes9cmdt473n6o96vh3xr7ym7qhct&rid=200w.gif)



*   Man Thinking about money and time after losing his net 

    [https://dribbble.com/shots/2519748-No-Time-No-Money/attachments/2519748-No-Time-No-Money?mode=media](https://dribbble.com/shots/2519748-No-Time-No-Money/attachments/2519748-No-Time-No-Money?mode=media)


Jobs Done

Name : Task Completed : Time

Anjessa Linger \



<table>
  <tr>
   <td><strong>Task Completed</strong>
   </td>
   <td><strong>Time</strong>
   </td>
  </tr>
  <tr>
   <td>Researching and creating presentation
   </td>
   <td> 2 hours
   </td>
  </tr>
  <tr>
   <td>Preparing for presentation
   </td>
   <td>1 day
   </td>
  </tr>
  <tr>
   <td>Helping Faith and Darlene with the demo video in the lab.
<p>
Powerpoint presentation with the app mockups, writing the script; instructions for the app “GearTrack”
   </td>
   <td> 1,5 hours
   </td>
  </tr>
  <tr>
   <td>Wrote save our ocean with help of Darlene
   </td>
   <td>3 hours
   </td>
  </tr>
  <tr>
   <td>Building the circuit and making the code with Darlene
   </td>
   <td>1 day
   </td>
  </tr>
  <tr>
   <td>Creating story and iterating 
   </td>
   <td>1 day
   </td>
  </tr>
  <tr>
   <td>Recording song with faith
   </td>
   <td>3 hours
   </td>
  </tr>
  <tr>
   <td>Speaking to animator and relaying information to make sure the story was correct
   </td>
   <td>6 hours
   </td>
  </tr>
  <tr>
   <td>Writing poem
   </td>
   <td>1 hour and 30 mins
   </td>
  </tr>
  <tr>
   <td>Assisting Faith with the video in Adobe premiere
   </td>
   <td>3hours
   </td>
  </tr>
</table>


 \
Darlene Gefferie


<table>
  <tr>
   <td><strong>Task Completed</strong>
   </td>
   <td><strong>Time</strong>
   </td>
  </tr>
  <tr>
   <td>App mockups: Homepage \
                	    Sign-up \
                	    Sign-in \
                	    Register gear \
                	    Add gear
   </td>
   <td> 2 days
   </td>
  </tr>
  <tr>
   <td>Coloring drawings from 31-59 in photoshop (Anjessa and Faith helped with making the drawings black and white in photoshop)
   </td>
   <td>1 day
   </td>
  </tr>
  <tr>
   <td>Helping faith and Anjessa with the demo video in the lab.
<p>
Powerpoint presentation with the app mockups, writing the script; instructions for the app “GearTrack”
   </td>
   <td> 1,5 hours
   </td>
  </tr>
  <tr>
   <td>Helped writing the song with Anjessa
   </td>
   <td>3 hours
   </td>
  </tr>
  <tr>
   <td>Building the circuit and making the code with Anjessa
   </td>
   <td>1 day
   </td>
  </tr>
  <tr>
   <td>Assisting Faith with the video in Adobe premiere
   </td>
   <td>3hours
   </td>
  </tr>
</table>


Yael Cairo


<table>
  <tr>
   <td>Coloring drawings from 25-45 in photoshop
   </td>
   <td>2 days
   </td>
  </tr>
</table>



## Demo / Proof of work


## Deliverables

< The deliverables of the project, so we know when it is complete>
- Project poster
- Proof of concept working
- Product pitch deck


## Project landing page/website (optional)


## the team & contact



# Contact


